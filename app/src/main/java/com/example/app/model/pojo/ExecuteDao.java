package com.example.app.model.pojo;

import android.content.Context;
import android.os.AsyncTask;

import com.example.app.model.DateTimeDao;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.example.app.common.MyApp.getAppContext;

public class ExecuteDao {

    private Context mContext;
    private DateTimeDao daoDateTime;


    public ExecuteDao() {
        mContext = getAppContext();
        daoDateTime = DateTimeDB.getInstance(mContext).getdateTimeDao();
    }

    public List<DateTime> listDateTime() {
        List<DateTime> list=null;
        try {
            list = new GetAllCarAsyncTask().execute().get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addDateTime(DateTime dateTime) {
        try {
            new AddCarAsyncTask().execute(dateTime).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            new DeleteDateTimeAsyncTask().execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAlarm(int id){
        try {
            new  DeleteAlarmAsyncTask().execute(id).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class GetAllCarAsyncTask  extends AsyncTask<Void, Void, List<DateTime>> {
        @Override
        protected List<DateTime> doInBackground(Void... voids) {
            return daoDateTime.dateTimeList();
        }
    }

    private class AddCarAsyncTask extends AsyncTask<DateTime, Void, DateTime> {

        @Override
        protected DateTime doInBackground(DateTime... dateTimes) {
            daoDateTime.insertCar(dateTimes[0]);
            return null;
        }
    }

    private class DeleteDateTimeAsyncTask extends   AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            daoDateTime.deleteAll();
            return null;
        }
    }

    private class DeleteAlarmAsyncTask extends  AsyncTask<Integer,Void,Void> {
        @Override
        protected Void doInBackground(Integer... ıntegers) {
            daoDateTime.deleteAlarm(ıntegers[0]);
            return null;
        }
    }
}
