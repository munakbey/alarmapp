package com.example.app.model.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import java.io.Serializable;

@Entity(tableName = "date-time")
public class DateTime implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;
    private Long milisecond;
    private String alarm_days;
    private Boolean alarm_mod;
    private int hour;
    private int minute;
    private String device_id;
    private String malarm_action;

    public DateTime(Long milisecond, String alarm_days, Boolean alarm_mod, int hour, int minute, String device_id, String malarm_action) {
        this.milisecond = milisecond;
        this.alarm_days = alarm_days;
        this.alarm_mod = alarm_mod;
        this.hour = hour;
        this.minute = minute;
        this.device_id = device_id;
        this.malarm_action = malarm_action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getMilisecond() {
        return milisecond;
    }

    public void setMilisecond(Long milisecond) {
        this.milisecond = milisecond;
    }

    public String getAlarm_days() {
        return alarm_days;
    }

    public void setAlarm_days(String alarm_days) {
        this.alarm_days = alarm_days;
    }
    
    public Boolean getAlarm_mod() {
        return alarm_mod;
    }

    public void setAlarm_mod(Boolean alarm_mod) {
        this.alarm_mod = alarm_mod;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMalarm_action() {
        return malarm_action;
    }

    public void setMalarm_action(String malarm_action) {
        this.malarm_action = malarm_action;
    }

}
