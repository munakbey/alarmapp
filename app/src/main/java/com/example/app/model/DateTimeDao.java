package com.example.app.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.app.model.pojo.DateTime;

import java.util.List;

@Dao
public interface DateTimeDao {

    @Insert
    void insertCar(DateTime dateTime);

    @Query("SELECT * FROM `date-time` order by hour asc , minute asc ")
    List<DateTime> dateTimeList();

    @Query("DELETE FROM `date-time`")
    void deleteAll();

    @Query("DELETE FROM `date-time` WHERE id = :id")
    void deleteAlarm(int id);
}
