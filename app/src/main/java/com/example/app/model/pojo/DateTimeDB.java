package com.example.app.model.pojo;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.example.app.model.DateTimeDao;

@Database(entities = {DateTime.class}, version = 5 ,exportSchema = false)
public abstract class DateTimeDB extends RoomDatabase {

    private static DateTimeDB instance;

    public static synchronized  DateTimeDB getInstance(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context,
                      DateTimeDB.class,"date-time")
                 /*   .addMigrations(MIGRATION_1_3)
                    .addMigrations(MIGRATION_1_4)*/
                    .addMigrations(MIGRATION_1_5)
                    .fallbackToDestructiveMigration()
                     .build();
        }
        return instance;
    }
/*
    static final Migration MIGRATION_1_3=new Migration(1,3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE driver ADD COLUMN alarm_id int");
            database.execSQL("ALTER TABLE driver ADD COLUMN date Date");
            database.execSQL("ALTER TABLE driver ADD COLUMN alarm_mod boolean");

        }
    };

    static final Migration MIGRATION_1_4=new Migration(1,4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("create table temp_table as select * from driver;");
            database.execSQL("drop table driver;");
            database.execSQL("create table driver (id INTEGER PRIMARY KEY, milisecond long, alarm_id string, date DATE , alarm_mod boolean,hour int , minute int);");
            database.execSQL("insert into driver select * from temp_table;");
            database.execSQL("drop table temp_table;");
        }
    };
*/
    static final Migration MIGRATION_1_5=new Migration(1,5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("create table temp_table1 as select * from driver;");
            database.execSQL("drop table driver;");
            database.execSQL("create table driver (id INTEGER PRIMARY KEY, milisecond long, alarm_id string,alarm_mod boolean,hour int , minute int," +
                    "device_id string,malarm_action string);");
            database.execSQL("insert into driver select * from temp_table1;");
            database.execSQL("drop table temp_table1;");
        }
    };



    public abstract DateTimeDao getdateTimeDao();

}
