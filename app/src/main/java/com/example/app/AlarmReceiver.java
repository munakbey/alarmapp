package com.example.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.example.app.common.MyApp;
import com.example.app.presenter.PDateTime;
import java.util.Calendar;
import java.util.TimeZone;

public class AlarmReceiver extends BroadcastReceiver {
    PDateTime pDateTime=new PDateTime();
    Fragment fr=new Fragment();
    Calendar cal=Calendar.getInstance();
    private int index;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("control","!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");

        Calendar cal2=Calendar.getInstance();
        cal2.setTimeZone(TimeZone.getDefault());
        Log.e("control",cal.get(Calendar.HOUR_OF_DAY)+"#!"+cal.get(Calendar.MINUTE));
        for (int i = 0; i <pDateTime.getDateTimeList().size(); i++) {
            if(cal.get(Calendar.HOUR_OF_DAY)==pDateTime.getDateTimeList().get(i).getHour() &&
                    cal.get(Calendar.MINUTE)==pDateTime.getDateTimeList().get(i).getMinute() ){
                index = i;
                break;
            }
        }

        String listString = "";
        String alarmID=String.valueOf(pDateTime.getDateTimeList().get(index).getAlarm_days());
        char[] alarmIdArr = alarmID.toCharArray();
        for (Object s :  alarmIdArr){
            listString += s;
        }
        Log.e("control",listString+" xxx");

        Log.e("control",cal2.getTime()+" <DAY");
        if(alarmIdArr[cal2.get(Calendar.DAY_OF_WEEK)-1]=='1' ){
            Toast.makeText(context, "Alarm Çalıyor! "+pDateTime.getDateTimeList().get(index).getDevice_id(), Toast.LENGTH_SHORT).show();
            Log.e("control","blablablablablabla");
        }//db den cekilen milisaniye eklenecek.

        long mili = cal2.getTimeInMillis();
        Calendar cal3 = Calendar.getInstance();
        cal3.setTimeInMillis(mili);
        Log.e("control", cal3.getTime() + " +++");
        cal2.add(Calendar.WEEK_OF_YEAR,1);
        Log.e("control",cal2.getTime()+" <-----");

        if (pDateTime.getDateTimeList().get(index).getAlarm_mod() == true) {
            cal2.set(cal2.get(Calendar.YEAR),
                    cal2.get(Calendar.MONTH),
                    cal2.get(Calendar.DAY_OF_MONTH),
                    pDateTime.getDateTimeList().get(index).getHour(), pDateTime.getDateTimeList().get(index).getMinute(), 0);

            Intent intent_ = new Intent(MyApp.getAppContext(), AlarmReceiver.class);//id gonder
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MyApp.getAppContext(), index, intent_, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

}