package com.example.app.presenter.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.R;
import com.example.app.common.MyApp;
import com.example.app.model.pojo.DateTime;
import com.example.app.presenter.PDateTime;
import com.example.app.view.activities.MainActivity;
import com.example.app.view.dialogs.AlarmDialog;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {
    Context mContext;
    PDateTime pDateTime=new PDateTime();
    public List list=new ArrayList<>();
    public static int deviceId;

    public MainAdapter(List list, Context mContext) {
        this.mContext = mContext;
        this.list=list;
    }

    @NonNull
    @Override
    public MainAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.main, parent, false);
        return new  MainAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.MyViewHolder holder, final int position) {
        Log.e("control","saaa"+list.get(position));
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmDialog exampleDialog = new AlarmDialog((MainActivity)mContext);
                deviceId=position;
                exampleDialog.show(  ((MainActivity)mContext).getSupportFragmentManager() , "alarm dialog");
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        Button btn;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            btn=itemView.findViewById(R.id.btn);
        }
    }
}
