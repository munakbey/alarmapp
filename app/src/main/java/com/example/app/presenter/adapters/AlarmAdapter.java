package com.example.app.presenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import com.example.app.R;
import com.example.app.common.MyApp;
import com.example.app.model.pojo.DateTime;
import com.example.app.presenter.PDateTime;
import com.example.app.view.activities.MainActivity;
import com.example.app.view.dialogs.AlarmDialog;
import com.example.app.view.dialogs.AlarmListDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.MyViewHolder> {
    public List<DateTime> listItem=new ArrayList<>();
    AlarmListDialog alarmListDialog;
    PDateTime pDateTime=new PDateTime();

    public AlarmAdapter(List<DateTime> listItem,  AlarmListDialog alarmListDialog) {
        this.listItem = listItem;
        this.alarmListDialog=alarmListDialog;
    }

    @NonNull
    @Override
    public AlarmAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(MyApp.getAppContext()).inflate(R.layout.alarms, parent, false);
        return new AlarmAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmAdapter.MyViewHolder holder, final int position) {
        Calendar cl = Calendar.getInstance();
        cl.setTimeInMillis(listItem.get(position).getMilisecond());

        String days = " ";
        holder.txtDeviceId.setText(listItem.get(position).getDevice_id());
        if(listItem.get(position).getAlarm_mod()==false) {
            holder.txtAlarmDate.setText(cl.get(Calendar.DAY_OF_MONTH) + "/" + cl.get(Calendar.MONTH) + "/" + cl.get(Calendar.YEAR));
        }else{
            for (int i = 0; i <7; i++) {
                if(listItem.get(position).getAlarm_days().charAt(i)=='1'){
                    switch (i){
                        case 0:
                            days+= MyApp.getAppContext().getString(R.string.sunday)+" ";
                            break;
                        case 1:
                            days+=MyApp.getAppContext().getString(R.string.monday)+" ";
                            break;
                        case 2:
                            days+=MyApp.getAppContext().getString(R.string.tuesday)+" ";
                            break;
                        case 3:
                            days+=MyApp.getAppContext().getString(R.string.wednesday)+" ";
                            break;
                        case 4:
                            days+=MyApp.getAppContext().getString(R.string.thursday)+" ";
                            break;
                        case 5:
                            days+=MyApp.getAppContext().getString(R.string.friday)+" ";
                            break;
                        case 6:
                            days+=MyApp.getAppContext().getString(R.string.saturday)+" ";
                            break;

                    }
                }
            }
            holder.txtAlarmDate.setText(days);
        }
        holder.txtAlarmTime.setText(listItem.get(position).getHour()+":"+listItem.get(position).getMinute());

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDateTime.deleteAlarm(listItem.get(position).getId());
                listItem=MyApp.executer().listDateTime();
                notifyDataSetChanged();
            }
        });

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // AlarmListDialog exampleDialog = new AlarmListDialog((MainActivity)mContext);
           //     exampleDialog.show(  ((MainActivity)mContext).getSupportFragmentManager() , "alarm dialog");

               // FragmentManager fragmentManager = ((MainActivity)mContext).getSupportFragmentManager();
                //FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                alarmListDialog.dismiss();
               // fragmentTransaction.commit();

                pDateTime.deleteAlarm(pDateTime.getDateTimeList().get(position).getId());

                listItem=MyApp.executer().listDateTime();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView  txtAlarmTime , txtAlarmDate,txtDeviceId;
        Button btnDelete,btnEdit;
        LinearLayout mLinearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtAlarmTime =itemView.findViewById(R.id.txt_alarm_time);
            txtAlarmDate=itemView.findViewById(R.id.txt_alarm_date);
            txtDeviceId=itemView.findViewById(R.id.txt_device_id);
            btnDelete=itemView.findViewById(R.id.btn_delete);
            btnEdit=itemView.findViewById(R.id.btn_edit);
            mLinearLayout=itemView.findViewById(R.id.mLinear_layout);
        }
    }
}
