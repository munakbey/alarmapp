package com.example.app.presenter;

import com.example.app.common.MyApp;
import com.example.app.model.pojo.DateTime;
import java.util.List;

public class PDateTime {

    public List<DateTime> getDateTimeList(){
        return MyApp.executer().listDateTime();
    }

    public void addDateTime(DateTime dateTime){
        MyApp.executer().addDateTime(dateTime);
    }

    public void deleteAlarm(int id){
        MyApp.executer().deleteAlarm(id);
    }
    public void deleteAll(){
        MyApp.executer().deleteAll();
    }
}
