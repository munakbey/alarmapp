package com.example.app.view.dialogs;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialogFragment;
import com.example.app.AlarmReceiver;
import com.example.app.R;
import com.example.app.common.MyApp;
import com.example.app.model.pojo.DateTime;
import com.example.app.presenter.PDateTime;
import com.example.app.presenter.adapters.MainAdapter;
import com.example.app.view.activities.MainActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class AlarmDialog extends AppCompatDialogFragment implements View.OnClickListener,TimePicker.OnTimeChangedListener {

    public Button btn_sun,btn_mon,btn_tues,btn_wed,btn_thu,btn_fri,btn_sat;
    public  static int buttonClick1,buttonClick2,buttonClick3,buttonClick4,buttonClick5,buttonClick6,buttonClick7;
    public  ArrayList arr=new ArrayList();
    public static int mMinute, mHour;
    ArrayList miliseconds=new ArrayList();
    Calendar cal = Calendar.getInstance();
    PDateTime pDateTime=new PDateTime();
    Button alarmList;
    Context mContext;

    public AlarmDialog(Context mContext) {
        this.mContext=mContext;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.alarm_dialog, null);

        Toast.makeText(MyApp.getAppContext(),""+ MainAdapter.deviceId,Toast.LENGTH_SHORT).show();

        builder.setView(view)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });


        TimePicker picker=view.findViewById(R.id.picker);
        picker.setIs24HourView(true);
        picker.setOnTimeChangedListener(this);

        Button btn_alarm=view.findViewById(R.id.btn_alarm);
        alarmList =view.findViewById(R.id.btn_alarm_list);
        btn_sun=view.findViewById(R.id.btn_sun);
        btn_mon=view.findViewById(R.id.btn_mon);
        btn_tues=view.findViewById(R.id.btn_tues);
        btn_wed=view.findViewById(R.id.btn_wed);
        btn_thu=view.findViewById(R.id.btn_thu);
        btn_fri=view.findViewById(R.id.btn_fri);
        btn_sat=view.findViewById(R.id.btn_sat);

        btn_sun.setOnClickListener(this);
        btn_mon.setOnClickListener(this);
        btn_tues.setOnClickListener(this);
        btn_wed.setOnClickListener(this);
        btn_thu.setOnClickListener(this);
        btn_fri.setOnClickListener(this);
        btn_sat.setOnClickListener(this);

        for (int i = 1; i <=7; i++) {
            arr.add(0);
        }

        alarmList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmListDialog exampleDialog = new AlarmListDialog(mContext);
                exampleDialog.show(getActivity().getSupportFragmentManager(), "alarm list dialog");
            }
        });

        btn_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long tmpMilisecond=updateTime(mMinute, mHour);
                miliseconds.add(tmpMilisecond);
                Log.e("control",updateTime(mMinute, mHour)+"  "+arr.size());

                String alarmId = "";
                for (Object s : arr) {
                    alarmId += s;
                }

                Log.e("control",alarmId+"%%%%%");
                if(alarmId.equals("0000000")) {    // DAILY ALARM
                    String dailyAlarmId = "";
                    for (int i = 0; i <7; i++) {
                        if(i+1==cal.get(Calendar.DAY_OF_WEEK)){
                            arr.set(i,1);
                            dailyAlarmId+="1";
                        }
                        dailyAlarmId+="0";
                    }
                    Log.e("control",dailyAlarmId+" dailyalarmid");
                    pDateTime.addDateTime(new DateTime(tmpMilisecond, dailyAlarmId, false, mHour, mMinute,String.valueOf(MainAdapter.deviceId),"alarmAction"));
                    for (int i = 0; i <arr.size(); i++) {
                        arr.set(i,0);
                    }
                }else{                     //WEEKLY ALARM
                    pDateTime.addDateTime(new DateTime(tmpMilisecond, alarmId, true, mHour, mMinute,String.valueOf(MainAdapter.deviceId),"alarmAction"));
                }
                setAlarm();
            }
        });

        return builder.create();
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        Calendar then=Calendar.getInstance();
        then.set(Calendar.HOUR_OF_DAY, hourOfDay);
        then.set(Calendar.MINUTE, minute);
        then.set(Calendar.SECOND, 0);

        mMinute=minute;
        mHour=hourOfDay;
        Log.e("control",hourOfDay+":"+minute);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sun:
                if(buttonClick1%2==1){
                    btn_sun.getBackground().setAlpha(1);
                    arr.set(0, 0);
                }else if(buttonClick1%2==0){
                    Log.e("control", "Sunday");
                    btn_sun.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(0, 1);
                }
                buttonClick1++;
                break;
            case R.id.btn_mon:
                if(buttonClick2%2==1){
                    btn_mon.getBackground().setAlpha(1);
                    arr.set(1, 0);
                }else if(buttonClick2%2==0) {
                    Log.e("control", "Monday");
                    btn_mon.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(1, 1);
                }
                buttonClick2++;
                break;
            case R.id.btn_tues:
                if(buttonClick3%2==1){
                    btn_tues.getBackground().setAlpha(1);
                    arr.set(2, 0);
                }else if(buttonClick3%2==0) {
                    Log.e("control", "Tuesday");
                    btn_tues.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(2, 1);
                }
                buttonClick3++;
                break;
            case R.id.btn_wed:
                if(buttonClick4%2==1){
                    btn_wed.getBackground().setAlpha(1);
                    arr.set(3, 0);
                }else if(buttonClick4%2==0) {
                    Log.e("control", "Wednesday");
                    btn_wed.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(3, 1);
                }
                buttonClick4++;
                break;
            case R.id.btn_thu:
                if(buttonClick5%2==1){
                    btn_thu.getBackground().setAlpha(1);
                    arr.set(4, 0);
                }else if(buttonClick5%2==0) {
                    Log.e("control", "Thursday");
                    btn_thu.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(4, 1);
                }
                buttonClick5++;
                break;
            case R.id.btn_fri:
                if(buttonClick6%2==1){
                    btn_fri.getBackground().setAlpha(1);
                    arr.set(5,0);
                }else if(buttonClick6%2==0) {
                    Log.e("control", "Friday");
                    btn_fri.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(5, 1);
                }
                buttonClick6++;
                break;
            case R.id.btn_sat:
                if(buttonClick7%2==1){
                    btn_sat.getBackground().setAlpha(1);
                    arr.set(6, 0);
                }else if(buttonClick7%2==0) {
                    Log.e("control", "Saturday");
                    btn_sat.setBackgroundColor(Color.parseColor("#DD00CCFF"));
                    arr.set(6, 1);
                }
                buttonClick7++;
                break;
        }
    }

    public long updateTime(int minute,int hour) {
        cal.setTimeZone(TimeZone.getDefault());
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH),
                hour, minute, 0);

        long timeMili = cal.getTimeInMillis();
        DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Log.e("control", timeMili + "----- " + simple.format(timeMili) + " / " + cal.get(Calendar.DAY_OF_WEEK) + " ###");

        return timeMili;
    }

    public void setAlarm(){
        for(int i = 0; i< miliseconds.size(); i++) {
            Intent intent = new Intent(MyApp.getAppContext(), AlarmReceiver.class);//id gonder
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MyApp.getAppContext(), i, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, (long)miliseconds.get(i),AlarmManager.INTERVAL_DAY , pendingIntent);
        }
    }

}
