package com.example.app.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.example.app.R;
import com.example.app.common.MyApp;
import com.example.app.presenter.PDateTime;
import com.example.app.presenter.adapters.AlarmAdapter;
import com.example.app.presenter.adapters.MainAdapter;
import com.example.app.view.dialogs.AlarmDialog;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
     Button btn,btn2,btn3;
     MainAdapter mainAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
   /*     btn=findViewById(R.id.btn);
        btn2=findViewById(R.id.btn2);
        btn3=findViewById(R.id.btn3);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });*/
        LayoutInflater inflater = getLayoutInflater();
    //    View view = inflater.inflate(R.layout.activity_main, null);

        final RecyclerView recyclerView = findViewById(R.id.main_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        List list=new ArrayList<>();
        list.add(0);    list.add(1);
        list.add(2);    list.add(3);

        mainAdapter= new MainAdapter(list,this);
        recyclerView.setAdapter(mainAdapter);

    }

    public void openDialog(){
        AlarmDialog exampleDialog = new AlarmDialog(MyApp.getAppContext());
        exampleDialog.show(getSupportFragmentManager(), "alarm dialog");
    }

}
