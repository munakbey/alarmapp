package com.example.app.view.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.app.R;
import com.example.app.common.MyApp;
import com.example.app.presenter.PDateTime;
import com.example.app.presenter.adapters.AlarmAdapter;

public class AlarmListDialog extends AppCompatDialogFragment  {
   // private AlarmDialog.ExampleDialogListener listener;
    AlarmAdapter alarmAdapter;
    PDateTime pDateTime=new PDateTime();
    Context mContext;
    public AlarmListDialog(Context mContext) {
        this.mContext=mContext;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.alarm_list_dialog, null);

        builder.setView(view)
                .setTitle(R.string.recent_alarms)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });

        final RecyclerView recyclerView = view.findViewById(R.id.alarms_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setHasFixedSize(true);

        alarmAdapter=new AlarmAdapter(pDateTime.getDateTimeList(),this/*,mContext*/);
        recyclerView.setAdapter(alarmAdapter);



        return builder.create();
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AlarmDialog.ExampleDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement MExampleDialogListener");
        }
    }

    public interface MExampleDialogListener {
        void applyTexts(String username, String password);
    }
*/

}
